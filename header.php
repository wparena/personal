<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package personal
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
  	<div class="brand">
  	<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
    		<img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.gif" />
    		&nbsp;<?php bloginfo( 'name' ); ?></a>
    		<?php dynamic_sidebar('menuwidget'); ?>
    </div>
   </div><br>
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>	
 
    </div>

    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
					'container'       => '',
   					'container_class' => 'menu-{menu slug}-container',
   					'container_id'    => '',
				    'menu_class'      => 'menu',
				    'menu_id'         => '',
				    'echo'            => true,
				    'fallback_cb'     => 'false',
				    'before'          => '',
				    'after'           => '',
				    'link_before'     => '',
				    'link_after'      => '',
				    'items_wrap'      => '%3$s',
				    'depth'           => 0,
				    'walker'          => ''
				) );
			?>

      </ul>
      
    </div>
  </div>
</nav>

	</header><!-- #masthead -->

	<div id="content" class="site-content">

#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Personal\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-04-02 05:40+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#. number of items in the mini cart.
#: inc/woocommerce.php:229
#, php-format
msgid "%d item"
msgid_plural "%d items"
msgstr[0] ""
msgstr[1] ""

#. used between list items, there is a space after the comma
#: inc/template-tags.php:52
msgid ", "
msgstr ""

#. Description of the theme
msgid ""
"3-column Personal WordPress resposive theme, loaded with two sidebars to "
"dislay content."
msgstr ""

#: functions.php:148
msgid "Add widgets here."
msgstr ""

#: comments.php:64
msgid "Comments are closed."
msgstr ""

#. 1: comment count number, 2: title.
#: comments.php:40
#, php-format
msgctxt "comments title"
msgid "%1$s thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#. %s: Name of current post
#. %s: Name of current post. Only visible to screen readers
#: functions.php:194 template-parts/content.php:43
#, php-format
msgid "Continue reading<span class=\"screen-reader-text\"> \"%s\"</span>"
msgstr ""

#. %s: Name of current post. Only visible to screen readers
#: inc/template-tags.php:89 template-parts/content-page.php:37
#, php-format
msgid "Edit <span class=\"screen-reader-text\">%s</span>"
msgstr ""

#. Author URI of the theme
msgid "http://themeicon.com"
msgstr ""

#: footer.php:19
msgid "https://wordpress.org/"
msgstr ""

#. URI of the theme
msgid "https://www.wpdesigner.com/themes/personal-wordpress-themes/"
msgstr ""

#: 404.php:21
msgid ""
"It looks like nothing was found at this location. Maybe try one of the links "
"below or a search?"
msgstr ""

#: template-parts/content-none.php:44
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#. %s: post title
#: inc/template-tags.php:72
#, php-format
msgid "Leave a Comment<span class=\"screen-reader-text\"> on %s</span>"
msgstr ""

#. used between list items, there is a space after the comma
#: inc/template-tags.php:59
msgctxt "list item separator"
msgid ", "
msgstr ""

#: functions.php:118
msgid "Main Sidebar"
msgstr ""

#: functions.php:101
msgid "Menu widget"
msgstr ""

#: 404.php:30
msgid "Most Used Categories"
msgstr ""

#: template-parts/content-none.php:14
msgid "Nothing Found"
msgstr ""

#. 1: title.
#: comments.php:34
#, php-format
msgid "One thought on &ldquo;%1$s&rdquo;"
msgstr ""

#: 404.php:17
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#: template-parts/content-page.php:24 template-parts/content.php:54
msgid "Pages:"
msgstr ""

#. Name of the theme
msgid "Personal"
msgstr ""

#. %s: post author.
#: inc/template-tags.php:35
#, php-format
msgctxt "post author"
msgid "by %s"
msgstr ""

#. %s: post date.
#: inc/template-tags.php:29
#, php-format
msgctxt "post date"
msgid "Posted on %s"
msgstr ""

#. 1: list of categories.
#: inc/template-tags.php:55
#, php-format
msgid "Posted in %1$s"
msgstr ""

#: functions.php:47
msgid "Primary"
msgstr ""

#. %s: CMS name, i.e. WordPress.
#: footer.php:21
#, php-format
msgid "Proudly powered by %s"
msgstr ""

#. 1: link to WP admin new post page.
#: template-parts/content-none.php:25
#, php-format
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#. %s: search query.
#: search.php:21
#, php-format
msgid "Search Results for: %s"
msgstr ""

#: functions.php:146
msgid "Sidebar"
msgstr ""

#: template-parts/content-none.php:38
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#. 1: list of tags.
#: inc/template-tags.php:62
#, php-format
msgid "Tagged %1$s"
msgstr ""

#. 1: Theme name, 2: Theme author.
#: footer.php:26
#, php-format
msgid "Theme: %1$s by %2$s."
msgstr ""

#. Author of the theme
msgid "ThemeIcon"
msgstr ""

#. %1$s: smiley
#: 404.php:47
#, php-format
msgid "Try looking in the monthly archives. %1$s"
msgstr ""

#: inc/woocommerce.php:227
msgid "View your shopping cart"
msgstr ""

#: functions.php:105 functions.php:122
msgid "Widgets in this area will be shown on menu."
msgstr ""
